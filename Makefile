TYPE = mdaPiano
ifeq ($(TYPE),mdaPiano)
	BUILD_TYPE = PIANO
else
	BUILD_TYPE = EPIANO
endif

BUNDLE = lv2-$(TYPE).lv2
PREFIX = /usr/local
INSTALL_DIR = $(PREFIX)/lib/lv2

SOURCES = src/*.cpp
FLAGS = -fPIC -DPIC -O $(WARNINGS) -D$(BUILD_TYPE)
WARNINGS = -g -Wall -pedantic

build/$(BUNDLE): src/$(TYPE)/manifest.ttl \
                 src/$(TYPE)/$(TYPE).ttl \
                 build/$(TYPE).so \
                 src/$(TYPE)/presets \
                 src/$(TYPE)/data/*.raw
	rm -rf build/$(BUNDLE)
	mkdir -p build/$(BUNDLE)
	cp -r $^ build/$(BUNDLE)

build/$(TYPE).so: $(SOURCES) src/$(TYPE).peg
	mkdir -p build
	$(CXX) $(FLAGS) -shared $(SOURCES) `pkg-config --cflags --libs lvtk-plugin-1` -o $@

src/$(TYPE).peg: src/$(TYPE)/$(TYPE).ttl
	ttl2c $^ src/$(TYPE).peg

install: build/$(BUNDLE)
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R build/$(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf build src/*.peg
